package divideandconquer;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MergeSorterTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		int[] nums = new int[]{4,3,2,1};
		int[] sorted = new int[] {1,2,3,4};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test
	public void testOddNumberElements() {
		int[] nums = new int[]{5,4,3,2,1};
		int[] sorted = new int[] {1,2,3,4,5};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test
	public void testManyUnsorted() {
		int[] nums = new int[]{5,11,6,0,-11,3,1,10,31,50,35,90,40,25,91};
		int[] sorted = new int[] {-11,0,1,3,5,6,10,11,25,31,35,40,50,90,91};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test
	public void testNegative() {
		int[] nums = new int[]{-5,-10,-6,-2,-11,-3};
		int[] sorted = new int[] {-11,-10,-6,-5,-3,-2};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test
	public void testOneElement() {
		int[] nums = new int[]{1};
		int[] sorted = new int[] {1};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test
	public void testNoElement() {
		int[] nums = new int[]{};
		int[] sorted = new int[]{};
		MergeSorter.mergeSort(nums);
		assertArrayEquals(sorted,nums);
	}
	@Test(expected = NoSuchElementException.class)
	public void testEmpty() {
		int[] nums = null;
		MergeSorter.mergeSort(nums);
	}

}
