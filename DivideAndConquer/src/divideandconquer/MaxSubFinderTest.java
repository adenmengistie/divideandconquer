package divideandconquer;
import divideandconquer.LinkedList.Node;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MaxSubFinderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void givenArrayTest() {
		int[] intArray = new int[]{13,-3,-25,-20,-3,-16,-23,18,
				20,-7,12,-5,-22,15,-4,7};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = 43;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenLinkedTest() {
		int[] intArray = new int[]{13,-3,-25,-20,-3,-16,-23,18,
				20,-7,12,-5,-22,15,-4,7};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = 43;
		assertEquals(expectedSum,res.getLast());
	}
	@Test
	public void givenOneElementArrayTest() {
		int[] intArray = new int[]{0};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = 0;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenOneElementLinkedTest() {
		int[] intArray = new int[]{0};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = 0;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenNegativeArrayTest() {
		int[] intArray = new int[]{-13,-3,-25,-20,-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = -3;
		assertEquals(expectedSum,res.getLast());
	}
	@Test
	public void givenNegativeLinkedTest() {
		int[] intArray = new int[]{-13,-3,-25,-20,-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = -3;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenPositiveArrayTest() {
		int[] intArray = new int[]{13,3,25,20,3,16,23,18,
				20,7,12,5,22,15,4,7};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = 213;
		assertEquals(expectedSum,res.getLast());
	}
	@Test
	public void givenPositiveLinkedTest() {
		int[] intArray = new int[]{13,3,25,20,3,16,23,18,
				20,7,12,5,22,15,4,7};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = 213;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenLeftArrayTest() {
		int[] intArray = new int[]{13,3,25,20,-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = 61;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenLeftLinkedTest() {
		int[] intArray = new int[]{13,3,25,20,-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = 61;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenRightArrayTest() {
		int[] intArray = new int[]{-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7,13,3,25,20};
		Triple<Integer,Integer,Integer> res = MaxSubFinder.getMaxSubarray(intArray);
		Integer expectedSum = 61;
		assertEquals(expectedSum,res.getLast());
	}
	
	@Test
	public void givenRightLinkedTest() {
		int[] intArray = new int[]{-3,-16,-23,-18,
				-20,-7,-12,-5,-22,-15,-4,-7,13,3,25,20};
		LinkedList list = new LinkedList(intArray);
		Triple<Node,Node,Integer> res = MaxSubFinder.getMaxSubList(list);
		Integer expectedSum = 61;
		assertEquals(expectedSum,res.getLast());
	}


}
