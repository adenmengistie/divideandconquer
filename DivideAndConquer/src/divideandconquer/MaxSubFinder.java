package divideandconquer;

import divideandconquer.LinkedList.Node;

public class MaxSubFinder {

	public static Triple<Integer,Integer,Integer> getMaxSubarray(int[] arr){
		return findMaxSubarray(arr,0,arr.length-1);
		
	}
	
	public static Triple<Node, Node,Integer> getMaxSubList(LinkedList list){
		return findMaxSublist(list,list.head,list.tail);
		
	}
	private static Triple<Integer,Integer,Integer> findMaxSubarray(int[] arr, int low, int high) {
		int mid; 
		
		if (high == low) 
			return (new Triple<Integer,Integer,Integer> (low,high,arr[low]));
		else {
			mid = (high + low)/2;
			Triple<Integer,Integer,Integer>leftSubarray = findMaxSubarray(arr,low,mid);
			Triple<Integer,Integer,Integer>rightSubarray = findMaxSubarray(arr,mid+1,high);
			Triple<Integer,Integer,Integer>midSubarray = findMaxCrossingarray(arr,low,mid,high);
				
			if(leftSubarray.getLast() >= rightSubarray.getLast() && leftSubarray.getLast() >= midSubarray.getLast())
				return leftSubarray;
			else if (rightSubarray.getLast() >= leftSubarray.getLast() && rightSubarray.getLast() >= midSubarray.getLast())
				return rightSubarray;
			else
				return midSubarray;	
		}
		
	}
	private static Triple<Integer,Integer,Integer> findMaxCrossingarray(int[] arr, int low, int mid, int high) {
			int sum = 0, leftIndex, rightIndex, leftSum, rightSum;
			
			leftIndex = mid;
			leftSum = arr[leftIndex];
			for (int i = mid; i >= low; i--) {
				sum += arr[i];
				if(sum > leftSum) {
					leftSum = sum;
					leftIndex = i;
				}
			}
			
			sum = 0;
			rightIndex = mid+1;
			rightSum = arr[rightIndex];
			for (int j = mid+1; j <= high; j++) {
				sum += arr[j];
				if(sum > rightSum) {
					rightSum = sum;
					rightIndex = j;
				}
			}
			sum = leftSum + rightSum;
			
			return (new Triple<Integer,Integer,Integer> (leftIndex,rightIndex,leftSum + rightSum));
	}
	private static Triple<Node, Node,Integer> findMaxSublist(LinkedList list, Node low, Node high) {
		Node mid;  
		
		if (high == low) 
			return (new Triple<Node,Node,Integer> (low,high,low.data));
		else {
			mid = list.middle(low, high);
			Triple<Node, Node, Integer> leftSublist = findMaxSublist(list,low,mid);
			Triple<Node, Node, Integer> rightSublist = findMaxSublist(list,mid.next,high);
			Triple<Node, Node, Integer> midSublist = findMaxCrossinglist(list,low,mid,high);
			
			if(leftSublist.getLast() >= rightSublist.getLast() && leftSublist.getLast() >= midSublist.getLast())
				return leftSublist;
			else if (rightSublist.getLast() >= leftSublist.getLast() && rightSublist.getLast() >= midSublist.getLast())
				return rightSublist;
			else
				return midSublist;	
		}
	}
	private static Triple<Node,Node,Integer> findMaxCrossinglist(LinkedList list, Node low, Node mid, Node high) {
		int sum = 0, leftSum, rightSum;
		Node index, leftIndex, rightIndex;
		

		leftIndex = mid;
		leftSum = leftIndex.data;
		index = leftIndex;
		while(index != null) {
			sum += index.data;
			if(sum > leftSum) {
				leftSum = sum;
				leftIndex = index;
			}
			index = index.prev;
		}
		
		sum = 0;
		rightIndex = mid.next;
		rightSum = rightIndex.data;
		index = rightIndex;
		while(index != null) {
			sum += index.data;
			if(sum > rightSum) {
				rightSum = sum;
				rightIndex = index;
			}
			index = index.next;
		}
		return (new Triple<Node,Node,Integer> (leftIndex,rightIndex,leftSum + rightSum));
}
}
