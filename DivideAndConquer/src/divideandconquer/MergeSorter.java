package divideandconquer;

import java.util.NoSuchElementException;

public class MergeSorter {
	
	public static void mergeSort(int[] arr) {
		
		if(arr == null) throw new NoSuchElementException("Array is empty.");
			
		sort(arr, 0, arr.length-1);
	}
	
	private static void sort(int[] arr, int start, int end) {
		int mid;
		
		if(start < end) {
			mid = (end + start)/2;
			sort(arr,start,mid);
			sort(arr,mid+1,end);
	
			merge(arr,start,mid,end);
		}
	}
	private static void merge(int[] arr, int start, int mid, int end) {
		int leftSize = mid - start + 1;
		int rightSize = end - mid;

		
		int[] left = new int[leftSize];
		int[] right = new int[rightSize];
		
		for(int lIndex = 0; lIndex < leftSize; lIndex++)
			left[lIndex] = arr[start+lIndex];
		for(int rIndex = 0; rIndex < rightSize; rIndex++)
			right[rIndex] = arr[mid+rIndex+1];
		
		int leftIndex = 0, rightIndex = 0;
		for (int mergeIndex = start; mergeIndex <= end; mergeIndex++) {
			if (rightIndex >= rightSize) {
				arr[mergeIndex] = left[leftIndex];
				leftIndex++;
			}
			else if (leftIndex >= leftSize) {
				arr[mergeIndex] = right[rightIndex];
				rightIndex++;
			}
			else if (left[leftIndex] < right[rightIndex]) {
				arr[mergeIndex] = left[leftIndex];
				leftIndex++;
			}
			else {
				arr[mergeIndex] = right[rightIndex];
				rightIndex++;
			}
		}
		
	}

}
